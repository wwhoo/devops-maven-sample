package io.kubesphere.devops;


import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Hello world!
 */
@RestController
@EnableAutoConfiguration
public class HelloWorldController {

    @RequestMapping("/")
    public String sayHello() {
        return "Really appreciate your star, that's the power of our life.";
    }
    @RequestMapping("/in")
    public String getIn(String str) {
        return str;
    }
    @RequestMapping("/you")
    public String you(String you) {
        if(you == null){
            return "输入的you为空！";
        }
        return you;
    }
    
}
